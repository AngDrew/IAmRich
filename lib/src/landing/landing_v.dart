import 'package:flutter/material.dart';

class LandingScreen extends StatefulWidget {
  @override
  _LandingScreenState createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();

    animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    );

    animation = Tween<double>(
      begin: 200.0,
      end: 220.0,
    ).animate(animationController);

    animation.addListener(() {
      if (animationController.isDismissed) {
        animationController.forward();
      } else if (animationController.isCompleted) {
        animationController.reverse();
      }
    });

    animationController.forward();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[900],
        title: const Text('I\'m Rich'),
        centerTitle: true,
      ),
      body: Container(
        width: size.width,
        height: size.height,
        alignment: Alignment.topLeft,
        child: AnimatedBuilder(
          animation: animation,
          builder: (BuildContext context, _) => Transform(
            transform: Matrix4.identity()
              ..translate(
                .0,
                animation.value,
              ),
            child: Image.asset(
              'assets/images/diamond.png',
            ),
          ),
        ),
      ),
    );
  }
}
